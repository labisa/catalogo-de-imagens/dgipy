Pacote dgi.excecoes
====================

Exceções utilizadas no `dgi`

Módulo dgi.excecoes
------------------------------

.. automodule:: dgi.excecoes
    :members:
    :undoc-members:
    :show-inheritance:
