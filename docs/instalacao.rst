Instalação
============

O dgipy pode ser instalado em sistemas operacionais Linux, Windows e MacOS com versões do Python 3.7 ou superior. Para tal, o seguinte comando pode ser utilizado

.. code:: shell

    $ pip install dgi

Instalação via código fonte
----------------------------

Caso queira é possível também baixar o projeto e fazer a instalação a partir do código fonte, para isto utilize os seguintes comandos

.. code:: shell

    $ git clone https://gitlab.com/labisa/catalogo-de-imagens/dgipy
    $ cd dgipy
    $ pip install -r requirements.txt
    $ python setup.py install
