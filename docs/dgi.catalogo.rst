Pacote dgi.catalogo
====================

Pacote para acesso ao catalogo do DGI/INPE

Módulo dgi.catalogo.catalogo
------------------------------

.. automodule:: dgi.catalogo.catalogo
    :members:
    :undoc-members:
    :show-inheritance:

Módulo dgi.catalogo.utils
---------------------------

.. automodule:: dgi.catalogo.utils
    :members:
    :undoc-members:
    :show-inheritance:
