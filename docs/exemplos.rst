Exemplos de utilização
========================

Nesta seção são apresentados exemplos de uso das funcionalidades presentes no `dgipy`

.. TIP::

    Para executar os comandos abaixo é necessário que você tenha o MongoDB instalado e o comando **mongod** esteja sendo executado.
    
    Caso tenha dúvidas sobre o MongoDB, consulte a página de :ref:`ConfiguracaoDB`

Criação do índice local de imagens
-------------------------------------

A primeira etapa necessária para utilizar o `dgipy` por completo é criar o índice local de imagens

Para fazer isto vamos utilizar a busca de imagens por região, onde iremos criar um polígono sobre todo o território nacional e requisitar a ferramenta que busque todas as imagens disponíveis no CBERS-4 para esta região.

.. code:: python

    from dgi.catalogo import CatalogoDGI
    
    data_de_busca = {"inicial": "01/01/2019", "final": "25/01/2019"}
    localizacoes = {
        "norte": "11.04024846",
        "sul": "-36.49917303", 
        "leste": "-30.77297895",
        "oeste": "-74.80618208"
    }

    catalogo = CatalogoDGI()
    catalogo.lista_imagens_dgi_regiao("CB4", "", data_de_busca, localizacoes)

Inicialmente faz-se a importação da classe **CatalogoDGI**, que popula o banco de dados local com as imagens presentes no catalogo do DGI/INPE. Após isto é feito a definição a definição das latitudes e longitudes da região buscada, sendo elas:

- norte: Latitude Norte;
- sul: Latitude Sul;
- leste: Longitude Leste;
- oeste: Longitude Oeste.

Após a definição das latitudes/longitudes, cria-se uma instância da classe de busca de imagens e através do método **lista_imagens_dgi_regiao** a busca por imagens do sensor **CBERS-4** é iniciada. Para mais informações de uso deste método consulte a página de descrição do método.

Buscas no índice local de imagens
----------------------------------

Como já citado, a etapa de criação do índice local de imagens basicamente salva as informações das imagens disponíveis no DGI/INPE, com este índice criado a busca fica rápida e simples. Esta seção apresenta um exemplo de busca no índice local de imagens

Em todos os exemplos apresentados abaixo será necessário a instância da classe de busca, como apresentado no trecho de código abaixo

.. code:: python 

    from dgi.acesso import BuscaDeImagemNoCatalogo

    catalogo_de_imagens_local = BuscaDeImagemNoCatalogo()


Com a instância de busca de imagens criadas, vejamos alguns exemplos de uso. Vamos começar com a busca por datas

.. code:: python

    imagens_na_data = catalogo_de_imagens_local.busca_imagens_por_data("01/01/2019", "01/03/2019")


Simples assim! Desta forma as imagens que estão dentro do intervalo de datas são selecionadas. Agora faremos a busca por quantidade de nuvem. Neste método de busca são passados 4 parâmetros, cada um destes representando a porcentagem máxima de nuvem em cada um dos quadrantes das imagens.

.. code:: python

    imagens_sem_nuvem = catalogo_de_imagens_local.busca_image_por_quantidade_de_nuvem(10, 10, 15, 15)

Podemos também fazer buscas utilizando o sensor desejado.

.. code:: python

    imagens_mux = catalogo_de_imagens_local.busca_imagens_por_instrumento("MUX")

Buscas com localizações também são possíveis, veja que, é possível buscas imagens que fazem intersecção com um determinado ponto, como é apresentado abaixo.

.. code:: python

    images_interseccao = catalogo_de_imagens_local.busca_imagens_por_intersec_ponto(lat=-4.37491, long=-67.80365)

Além disto pode-se buscar também por imagens utilizando um shape, veja

.. code:: python

    imagens_no_shape = catalogo_de_imagens_local.busca_imagens_dentro_do_shape([[[-63.185658, -14.595427],
                                                                                [-61.793346, -14.595427],
                                                                                [-61.793346, -15.834096],
                                                                                [-63.185658, -15.834096],
                                                                                [-63.185658, -14.595427]]])

Por fim, a busca também pode ser feita por orbita-ponto, veja:

.. code:: python

    imagens_na_ob = catalogo_de_imagens_local.busca_imagens_por_orbita_ponto(orbita=100, ponto=200)

Para ver outras consultas disponíveis consulte a página do módulo **dgi.acesso.busca**.

Realizando pedidos de imagens
-------------------------------

Anteriormente foi apresentado como as buscas de imagens já indexadas pelo `dgipy` devem ser consultadas, mas e após a consulta, o que devemos fazer com as imagens retornadas ? Simples, faremos pedidos utilizando as imagens encontradas! Vejamos como fazer isto.

.. code:: python

    from dgi.acesso import BuscaDeImagemNoCatalogo, FacilitaDGI

    facilita_dgi = FacilitaDGI("SEU_USUARIO_DO_CATALOGO", "SUA_SENHA_DO_CATALOGO")
    catalogo_de_imagens_local = BuscaDeImagemNoCatalogo()

    # Fazendo a busca
    imagens_mux = catalogo_de_imagens_local.busca_imagens_por_instrumento("MUX")

    # Realizando o pedido com as imagens encontradas
    informacoes_do_pedido = facilita_dgi.realiza_pedido(imagens_mux)

Após realizar o pedido, o *link* de aquisição das imagens já estará disponível em seu *e-mail* de cadastro no catálogo.

.. NOTE::

    A biblioteca permite apenas 200 imagens por pedido.


Baixando imagens de pedidos
-----------------------------

Para baixar as imagens é necessário ter realizado o pedido. Com o pedido realizado você terá um *link*, para baixar as imagens utilizando o `dgipy` basta pegar este *link* e inserir na função de aquisição, como apresentado abaixo

.. code:: python

    from dgi.acesso import BuscaDeImagemNoCatalogo, FacilitaDGI

    facilita_dgi = FacilitaDGI("SEU_USUARIO_DO_CATALOGO", "SUA_SENHA_DO_CATALOGO")
    imagens_ja_baixadas = facilita_dgi.baixa_imagens_de_link_do_pedido("LINK_DO_PEDIDO", "LOCAL_ONDE_SERA_SALVO")

O objeto retornado pelo método **baixa_imagens_de_link_do_pedido** contém uma relação com todas as imagens já baixadas pelo usuário. É possível também fazer buscas no grupo de imagens já baixadas, veja na seção seguinte.

Buscando imagens já baixadas
-------------------------------

Com o tempo de uso pode ser que você acumule muitas imagens já baixadas, por conta disto o `dgipy` salva as imagens já baixadas pelo usuário no banco de dados. Assim há uma classe de consulta das imagens já baixadas. Esta seção apresenta exemplos de consultas das imagens já baixadas.

Assim como apresentado na busca de imagens no catalogo local, aqui também é necessário criar uma instância da classe de busca, veja como é feito no código apresentado abaixo.

.. code:: python

    from dgi.acesso import BuscaImagensJaBaixadas

    imagens_salvas = BuscaImagensJaBaixadas()

Com isto, vamos começar a fazer as buscas. Vejamos como recuperar todas a relação de todas as imagens já baixadas.

.. code:: python

    todas_as_imagens = imagens_salvas.recupera_todas_as_imagens_baixadas()

Este método retorna um *DataFrame* Pandas com todas as imagens já baixadas. É possível também fazer filtros, veja abaixo a busca de imagens já baixadas por instrumento.

.. code:: python

    imagens_baixadas_mux = imagens_salvas.recupera_todas_as_imagens_baixadas_por_instrumento("MUX")
