Serviços
==========

Para o `dgipy` sempre buscar as últimas imagens para o usuário, é necessário que o banco de dados local esteja atualizado com todos os índices de imagens presentes no DGI/INPE, para isto alguns serviços foram criados para facilitar a automatização da busca

Windows
---------

A utilização do serviço do Windows deverá ser feita através do agendamento de tarefas do Windows. Para fazer esta configuração, vamos inicialmente criar um *script* Python com o código de execução do serviço.

.. code:: python

    from dgi.servicos.windows import servico_dgi_windows

    servico_dgi_windows()

Com o código dentro de algum *script* será necessário apenas criar um *script* *batch* com seu comando python para a execução do *script* python.

Desta forma o que teremos será dois arquivos:

- servico.py: Contém o código python para a inicialização do serviço de atualização da base de dados;
- executa.bat: Código com o comando **cmd** de execução do script **servico.py**.

Veja que estes nomes são arbitrários

.. TIP::
    A estrutura do arquivo `executa.bat`, deve ficar parecida com a demonstrada abaixo

    .. code::

        @echo off

        python servico.py

    É importante que o *script* `executa.bat` esteja no mesmo diretório do *script* `servico.py`.

Feito as configurações acima, agende a execução da tarefa no Windows que execute o *script* `executa.bat`. O período de execução definido no agendamento será a taxa de atualização do banco de dados.

Linux
-------

Para a execução no Linux basta importar a função de serviço e executar, neste momento um **Daemon** será criado no sistema, veja o exemplo de código abaixo

.. code:: python

    from dgi.servicos.windows import servico_dgi_linux

    servico_dgi_linux()

.. TIP::

    Configure a execução deste código a cada inicialização do sistema, para evitar problemas com o atrazo das atualizações.
