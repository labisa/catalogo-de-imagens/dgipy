.. _ConfiguracaoDB:

Configuração do banco de dados
================================

O `dgipy` trabalha utilizando o MongoDB 3.4 para armazenar todas as informações do DGI/INPE e tornar mais fácil e rápido a busca de imagens. Assim para a utilização do `dgipy` será necessário configurar o MongoDB. Esta seção apresenta as formas com que as configurações de acesso devem ser apresentadas durante o uso da aplicação.

Caso esteja com dúvida sobre a instalação do banco de dados utilizado pelo dgipy (MongoDB), consulte as :ref:`InstalacaoMongoDB`

Sobre a utilização
---------------------

Nesta seção será apresentado como as informações de acesso devem ser utilizadas nas classes que necessitam deste tipo de informação durante sua instância. A lista abaixo indica cada uma das classes e funções que utilizam as informações de acesso ao banco.

- dgi.acesso.busca.BuscaDeImagemNoCatalogo;
- dgi.acesso.busca.BuscaImagensJaBaixadas;
- dgi.acesso.facilidade.FacilitaDGI;
- dgi.catalogo.catalogo.CatalogoDGI;
- dgi.servicos.linux.servico_dgi_linux;
- dgi.servicos.windows.servico_dgi_windows.

Estas classes e funções durante sua utilização podem receber o atributo `db_props`, que representa um dicionário com as informações de acesso ao banco. Vamos a uma descrição de cada uma das chaves deste dicionário.

.. code:: python

    db_props = {
        "host": "Endereço IP do banco de dados",
        "porta": "Porta do banco de dados",
        "usuario": "Usuário para utilizar o banco (Opcional),
        "senha": "Senha para utilizar o banco (Opcional)"
    }

A tabela abaixo apresenta os tipos e necessidades de cada uma das chaves

    +---------+---------+-------------+
    | Chave   | Tipo    | Obrigatório |
    +---------+---------+-------------+
    | host    | String  | Sim         |
    +---------+---------+-------------+
    | porta   | Integer | Sim         |
    +---------+---------+-------------+
    | usuario | String  | Não         |
    +---------+---------+-------------+
    | senha   | String  | Não         |
    +---------+---------+-------------+

Veja que, para as chaves `usuario` e `senha` não há obrigatoriedade, assim, eles deve ser utilizados apenas quando o banco possui alguma autenticação.


Como utilizar
----------------

Bem agora que entendemos como são as configurações de acesso ao banco de dados, vejamos onde estas configurações são utilizadas. 

Na subseção foram apresentadas as classes e funções que consomem as configurações, desta forma é necessário apenas no momento da utilização especificar as configurações, veja no exemplo abaixo, que utiliza a classe **FacilitaDGI**.

.. code:: python

    from dgi.acesso import FacilitaDGI

    facilita_dgi = FacilitaDGI("SEU_USUARIO_DO_CATALOGO", "SUA_SENHA_DO_CATALOGO", db_props= {
        "host": "192.168.0.1",
        "porta": 21017
    })

E este mesmo padrão deve ser utilizada nas demais classes. Nas funções a utilização é a mesma, veja o exemplo utilizando a função do serviço do Windows.


.. code:: python

    from dgi.servicos.windows import servico_dgi_windows
    servico_dgi_windows(db_props = {
        "host": "10.20.30.2",
        "porta": 21027
    })

.. NOTE::

    Caso você tenha feito uma instalação padrão do MongoDB 3.4 em sua máquina, não é necessário especificar o `db_props` em nenhum momento do código

Criando estrutura do banco
---------------------------

Bem agora que já sabemos como realizar conexões com o banco de dados, teremos de importar um *script* de configuração da estrutura do banco de dados.

Este *script* está dentro do diretório `db`, existindo a verão para Windows (bat) e para Linux (Shell). Basta acessar o diretório e executar o *script*.

.. NOTE::

    Caso haja configurações de endereço ou mesmo de autenticação, você terá de editar o *script* de configuração de banco de dados.

.. TIP::

    Não esqueça de executar o *script* de configuração através do *cmd* ou terminal, de acordo com seu sistema operacional.

Estrutura do banco de dados
-----------------------------

Para entender melhor como o banco de dados está estruturado, veja as tabelas abaixo, nelas estão sendo representados os elementos das duas coleções disponíveis no banco de dados

- Coleção `catalogo`

Esta é a coleção que contém o índice de imagens do DGI recuperadas pelo `dgipy`.

+-------------------------------+---------+
| Nome do campo                 | Tipo    |
+-------------------------------+---------+
| nome                          | String  |
+-------------------------------+---------+
| quicklook_url                 | String  |
+-------------------------------+---------+
| satelite                      | String  |
+-------------------------------+---------+
| instrumento                   | String  |
+-------------------------------+---------+
| orbita                        | Integer |
+-------------------------------+---------+
| ponto                         | Integer |
+-------------------------------+---------+
| data                          | ISODate |
+-------------------------------+---------+
| quantidade_nuvens.quadrante_1 | Integer |
+-------------------------------+---------+
| quantidade_nuvens.quadrante_2 | Integer |
+-------------------------------+---------+
| quantidade_nuvens.quadrante_3 | Integer |
+-------------------------------+---------+
| quantidade_nuvens.quadrante_4 | Integer |
+-------------------------------+---------+
| pagina                        | Integer |
+-------------------------------+---------+
| centro_da_cena.coordinates    | List    |
+-------------------------------+---------+
| poligono_da_cena              | List    |
+-------------------------------+---------+
| local                         | String  |
+-------------------------------+---------+

- Coleção `imagens_adquiridas`

Coleção com as informações das imagens já baixadas na máquina do usuário

+------------------------+---------+
| Nome do campo          | Tipo    |
+------------------------+---------+
| satelite               | String  |
+------------------------+---------+
| instrumento            | String  |
+------------------------+---------+
| data                   | ISODate |
+------------------------+---------+
| orbita                 | Integer |
+------------------------+---------+
| ponto                  | Integer |
+------------------------+---------+
| local                  | String  |
+------------------------+---------+
| data_insercao_no_banco | String  |
+------------------------+---------+

.. _InstalacaoMongoDB:

Notas para instalação do MongoDB 3.4
-------------------------------------

Esta seção apresenta as etapas para a instalação do MongoDB nos sistemas operacionais Windows e  Ubuntu 18.04.

Caso seu sistema operacional não esteja aqui, consulte o `manual do MongoDB <https://docs.mongodb.com/manual/installation/>`_.

* Instalação no Ubuntu 18.04

Para realizar a instalação do MongoDB 3.4 no Ubuntu 18.04, basta seguir os passos descritos abaixo. 

Primeiro faça a atualização dos pacotes e instale o `gnupg`.

.. code:: shell

    sudo apt update -y && sudo apt install gnupg -y 

Feito isto, registre a chave para acesso ao repositório de pacotes do MongoDB.

.. code:: shell

    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6

Com a chave registrada, adicione o repositório do MongoDB em sua máquina

.. code:: shell

    sudo echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list

Por fim, faça a atualização dos pacotes e a instalação do MongoDB.

.. code:: shell

    sudo apt update && sudo apt install mongodb-org -y

.. note::

    Com o MongoDB instalado é necessário fazer sua execução, para tal utilize um dos seguintes comandos
    - service mongod start
    - mongod

    Lembre-se que é necessário o serviço do MongoDB iniciado para fazer sua utilização com o dgipy.


* Instalação no Windows

Para realizar a instalação do MongoDB 3.4 no Windows, basta seguir os passos abaixo. 

.. note::

    A instação apresentadas abaixo é feita no Windows 10, porém também são válidas para as versões anteriores.

O primeiro passo para a instalação do MongoDB é fazer seu *download*, para isso, acesse a `página de download do MongoDB <https://www.mongodb.com/download-center/community?jmp=docs>`_

.. image:: res/install_mongo_windows_1.gif
  :alt: Instação Windows Mongo (1)

Com o *download* realizado, faça a execução do arquivo baixado e siga os passo para a instalação.

.. image:: res/install_mongo_windows_2.gif
  :alt: Instação Windows Mongo (2)

Feito a instalação é necessário configurar os executáveis do MongoDB. Para isso, acesse o diretório **bin** que está no diretório onde o MongoDB foi instalado, uma vez acessado o diretório, copie seu caminho e faça sua inserção na variável de ambiente **path**.

.. image:: res/install_mongo_windows_3.gif
  :alt: Instação Windows Mongo (3)

.. note::

    No exemplo acima, o caminho completo é **C:\\Program Files\\MongoDB\\Server\\3.4\\bin**, caso você tenha feito a instalação padrão, provavelmente este também será o seu caminho para os binários do MongoDB.

Agora, antes de executar o MongoDB, vá até o disco **C:\\** e crie o diretório **data**, dentro deste crie o diretório **db**, gerando a seguinte estrutura **C:\\data\\db**.

Pronto! agora o MongoDB está pronto para ser executado. Vá até o CMD e execute o comando **mongod**.

.. image:: res/install_mongo_windows_4.gif
  :alt: Instação Windows Mongo (4)

.. tip::

    Tanto no Windows quanto no Linux é necessário manter o **mongod** sendo executado para os trabalhos com o banco de dados.
