.. dgi documentation master file, created by
   sphinx-quickstart on Sat Feb 16 16:32:26 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentação do dgipy 0.8.6
===============================

O dgipy é uma biblioteca criada para facilitar a busca e aquisição de imagens através do catálogo do DGI/INPE.

.. toctree::
   :maxdepth: 4

   intro
   instalacao
   configuracao-banco-de-dados
   exemplos
   servicos
   dgi
