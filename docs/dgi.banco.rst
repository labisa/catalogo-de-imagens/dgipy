Módulos extras
================

Módulos extras criados para facilitar o desenvolvimento e utilização do `dgipy`

banco
------

.. automodule:: dgi.banco
    :members:
    :undoc-members:
    :show-inheritance:

utils
---------
.. automodule:: dgi.utils
    :members:
    :undoc-members:
    :show-inheritance:

decoradores
--------------

.. automodule:: dgi.decoradores
    :members:
    :undoc-members:
    :show-inheritance:

