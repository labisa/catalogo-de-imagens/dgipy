Pacote dgi.servicos
====================

Módulo dgi.servicos.linux
----------------------------

.. automodule:: dgi.servicos.linux
    :members:
    :undoc-members:
    :show-inheritance:


Módulo dgi.servicos.windows
----------------------------
.. automodule:: dgi.servicos.windows
    :members:
    :undoc-members:
    :show-inheritance:
