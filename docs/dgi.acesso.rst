.. _DGIAcesso:

Pacote dgi.acesso
==================

Pacote com módulos que facilitam a busca de imagens, realização de pedidos e *downloads*

.. _DGIAcessoBusca:

Módulo dgi.acesso.busca
-------------------------

.. automodule:: dgi.acesso.busca
    :members:
    :undoc-members:
    :show-inheritance:

Módulo dgi.acesso.facilidade
------------------------------

.. automodule:: dgi.acesso.facilidade
    :members:
    :undoc-members:
    :show-inheritance:

Módulo dgi.acesso.utils
-------------------------

.. automodule:: dgi.acesso.utils
    :members:
    :undoc-members:
    :show-inheritance:
