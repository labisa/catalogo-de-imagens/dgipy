Sobre o projeto
================

O `dgipy` é uma biblioteca criada para facilitar a busca e aquisição de imagens do catálogo do DGI/INPE. O projeto foi inicialmente desenvolvido para facilitar o trabalho com dados CBERS-4, porém atualmente está sendo expandido para buscar todos os sensores presentes no catálogo do DGI/INPE.

Fluxo geral de funcionamento
------------------------------

Para seu funcionamento o dgipy realiza o consumo de todos os serviços e informações disponiblizados pelo catálogo DGI/INPE, faz isto através da raspagem de dados das páginas do catálogo.

De maneira geral a ideia do dgipy é simples. Fazer a aquisição da informação de todas as imagens que estão disponíveis no catálogo da DGI/INPE e salvar essas em um banco de dados, criando assim um índice local de informações. Isto é feito para que, todas as operações de busca passam a ser independentes do DGI, o que permite buscas tão rápidas quanto a do próprio serviço, porém através de uma interface programável.

Desta forma, o funcionamento do dgipy é dividido em três grupos: (i) Criação do índice local de imagens; (ii) Buscas de imagens no índice local e realização de pedidos; (iii) Aquisição de imagens presentes nos pedidos.

Cada uma dessas etapas são descritas nas subseções abaixo.

Criação do índice local de imagens
-----------------------------------

Para a geração do índice local de imagens, o dgipy recebe informações do usuário, essas descrevendo o local e as datas das imagens que devem ser pesquisadas no DGI/INPE e inseridas no banco de dados local, como apresentado abaixo.

.. image:: res/diagrama_geral_1.svg
  :width: 800
  :alt: Diagrama geral do dgipy (1)

Perceba que, como citado anteriormente, o índice local de imagens, apresentado no diagrama como **Banco de imagens dgipy**, possui apenas informações sobre as imagens que estão disponíveis no DGI/INPE, informações essas relacionadas a localização da imagem, sensor imageador, data da aquisição e diversas outras informações, que podem ser melhor compreendidas na seção de descrição do banco de dados.

Buscas de imagens no índice local e realização de pedidos
----------------------------------------------------------

Com o índice local de imagens gerado, é possível requisitar ao dgipy que ele realize buscar sobre esse índice, permitindo assim que exista um filtro de imagens antes da realização dos pedidos. O índice das imagens encontradas nesta busca ficam salvas temporariamente para serem utilizadas no registro de pedidos.

As pesquisas disponíveis no dgipy buscam reproduzir ao máximo o sistema de buscas do DGI/INPE. Cada um dos métodos disponíveis para a pesquisa no índice de imagens são descritas no :ref:`DGIAcessoBusca`

.. image:: res/diagrama_geral_2.svg
  :width: 800
  :alt: Diagrama geral do dgipy (2)

Após a realização do filtro de imagens é possível realizar pedidos com tais imagens. O ponto aqui é que, o dgipy acessa os serviços do DGI/INPE e registra todas as imagens que foram obtidas na pesquisa. Seu fluxo de funcionamento é apresentado abaixo.

.. image:: res/diagrama_geral_3.svg
  :width: 800
  :alt: Diagrama geral do dgipy (3)

Por fim, com o pedido feito, o link para a aquisição das imagens no pedido deve chegar no email da conta utilizada para fazer o pedido.

Aquisição de imagens presentes nos pedidos
-------------------------------------------

Recebendo o link com as imagens do pedido, é possível utilizar o dgipy para fazer a aquisição das imagens. Neste passo o link recebido é tratado pelo dgipy e todas as imagens presentes no mesmo são baixadas.

Aqui, para cada imagem baixada é criada um registro no banco de dados, o que permite ao usuário fazer buscar no banco de dados local, facilitando também seu processo de consumo das imagens baixadas.

.. image:: res/diagrama_aquisicao_imagens.svg
  :width: 800
  :alt: Diagrama de aquisição de imagem

Fluxo geral de funcionamento
-----------------------------

Todo o fluxo descrito acima é sumarizado através da Figura abaixo. 

.. image:: res/diagrama_geral.svg
  :width: 800
  :alt: Diagrama geral do dgipy (4)