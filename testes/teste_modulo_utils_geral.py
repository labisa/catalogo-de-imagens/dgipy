from unittest import TestCase

from dgi.excecoes import ArquivoDeConfiguracaoIncorreto

from dgi.utils import (define_ambiente_de_processamento, 
                        carregar_arquivo_de_configuracao_do_banco_de_dados)

class TesteUtilsGeral(TestCase):
    """Teste do `utils` geral do pacote dgi-dumps
    """

    def setUp(self):
        with open("teste.txt", "w") as txt:
            txt.write('''[BANCO_DE_DADOS]
HOST = "127.0.0.1"
PORTA = 27017
USUARIO = "USUARIO_DO_BANCO"
SENHA = "SENHA_DO_BANCO"
''')

        with open("teste2.txt", "w") as txt:
            txt.write("")

    def tearDown(self):
        import os
        os.remove("teste.txt")
        os.remove("teste2.txt")

    def teste_definicao_do_ambiente_de_processamento(self):
        
        import platform
        from threading import Thread
        from multiprocessing import Process

        if platform.system() == "Windows":
            ambiente_esperado = Thread
        else:
            ambiente_esperado = Process
        ambiente_escolhido = define_ambiente_de_processamento()

        self.assertEqual(ambiente_esperado, ambiente_escolhido)

    def teste_leitura_de_arquivo_de_configuracao_de_bd(self):
        arquivo = carregar_arquivo_de_configuracao_do_banco_de_dados("teste.txt")

        self.assertEqual({"host", "porta", "usuario", "senha"}, set(arquivo))

    def teste_leitura_de_arquivo_de_configuracao_de_bd_invalido(self):
        self.assertRaises(ArquivoDeConfiguracaoIncorreto, 
                carregar_arquivo_de_configuracao_do_banco_de_dados, "teste2.txt") 
