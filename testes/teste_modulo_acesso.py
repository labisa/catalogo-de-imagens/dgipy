from unittest import TestCase

import dgi.acesso.utils as acesso_utils


class TesteUtils(TestCase):
    """Teste do `utils` do módulo de acesso do dgi-dumps
    """

    def setUp(self):
        self.mock_imagens_duplicadas = [
            {
                "nome": "IMAGEM_A",
                "pagina": 2,
                "orbita": "173",
                "ponto": "120"
            }, {
                "nome": "IMAGEM_A",
                "pagina": 2,
                "orbita": "173",
                "ponto": "120"
            }, {
                "nome": "IMAGEM_B",
                "pagina": 2,
                "orbita": "173",
                "ponto": "120"
            }
        ]
        self.lista = [1, 2, 3, 4, 5]
        self.data = "21-02-1991"

    def tearDown(self):
        self.mock_imagens_duplicadas = None

    def test_funcao_de_remocao_de_elementos_duplicados(self):
        imagens_sem_duplicacao = acesso_utils.remove_imagens_duplicadas(
            self.mock_imagens_duplicadas
        )

        self.assertEqual(len(imagens_sem_duplicacao), 2)

    def test_funcao_de_divisao_de_lista(self):
        lista_dividida = acesso_utils.divide_lista(self.lista, 2)

        self.assertEqual(len(lista_dividida), 2)

    def test_funcao_de_transformacao_de_data(self):
        from datetime import datetime
        data_nova = acesso_utils.string_para_data(self.data, sep='-')

        self.assertEqual(type(data_nova), datetime)
