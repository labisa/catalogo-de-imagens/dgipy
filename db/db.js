/**
 * Script para a configuração do banco de dados do dgi_dumps
 */

db.createCollection("imagens_adquiridas");
db.createCollection("catalogo");
// Índices para manter a geobusca com velocidades aceitáveis
db.catalogo.createIndex({centro_da_cena: "2dsphere"});
db.catalogo.createIndex({poligono_da_cena: "2dsphere"});

// Índice para evitar elementos com nomes duplicados 
db.catalogo.ensureIndex({"nome": 1}, {unique: true})
